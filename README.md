## Mods

# Ako si stiahnem a nainštalujem módy?
1. Klikni vpravo hore na button naľavo od `Clone`
![Step1](img/tut1.png?raw=true "Title")
2. Z drop-down baru, ktorý sa po kliknutí objavil vyber `zip`
![Step2](img/tut2.png?raw=true "Title2")
3. Obsah stiahnutého `.zip` archívu extrahuj do zložky `mods` v adresári tvojho minecraftu (obvykle `/AppData/Roaming/.minecraft/mods`)

# Zoznam povinných módov a k čomu sú treba
- **Optifine** na využívanie shaderov
- **Chisel and bits** detailnejšia úprava blokov
- **mclib, metamorph a blockbuster** replaye postáv a hercov
- **Aperture** pre lepšiu kameru a prehrávanie blockbuster scén

# Tieto módy niesú potreba pre pripojenie na server, ale budú použité pri natáčaní
- **CMD Cam a Creative Core** na kamerovanie
- **Real First Person** je vidieť telo hráča aj vo first person
- **Forgelin a BetterFoliage** pre krajšiu vegetáciu
- **Sound Physics** pre lepšie zvuky

# Momentálne nepoužívané
- **Conquest Reforged** pre 4000 ďalších modelov blokov
